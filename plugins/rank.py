import os.path, sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))
import db
import psycopg2
import re

class Rank(object):


    def handleMessage(self, data, s):
        username = data["user"]

        if "subtype" in data:
            print "CHANGED MESSAGE WAS NOT HANDLED CORRECTLY"
        else:
            with db.get_connection() as conn:
                with conn.cursor() as curs:
                    curs.execute("SELECT * FROM scores WHERE username = (%s);", (username,))
                    old_entry = curs.fetchone()
                    old_rank = 0
                    if old_entry is not None:
                        old_rank = old_entry[2]
                    try:
                        curs.execute("""INSERT INTO scores VALUES (%(username)s, 0) ON CONFLICT (username) DO UPDATE 
                                         SET score = scores.score + 1, rank = floor(0.25 * sqrt(scores.score))
                                        WHERE scores.username = %(username)s;""", {"username": username})
                    except psycopg2.Error as e:
                        print e.pgerror
                    curs.execute("SELECT * FROM scores WHERE username = (%s);", (username,))
                    new_rank = curs.fetchone()[2]

                    if new_rank > old_rank:
                        s.send_msg("Rank Up! %s has reached Rank %d" % (username, new_rank), channel_name=data["channel"], confirm=False)
