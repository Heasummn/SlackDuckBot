import os.path, sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))
import db
import psycopg2
import re

class SimpleLogger(object):

    p = re.compile(ur'\<(.*?)\>')

    """CREATE TABLE IF NOT EXISTS textarchive (
     pk    SERIAL PRIMARY KEY,
     message   text NOT NULL
    );"""

    def __init__(self):
        with db.get_connection() as conn:
            with conn.cursor() as curs:
                try:
                    curs.execute("""CREATE TABLE IF NOT EXISTS textarchive (
                                     pk    SERIAL PRIMARY KEY,
                                     message   text NOT NULL
                                    );""")
                except psycopg2.Error as e:
                    print e.pgerror


    def replaceUrlTags(self, message, matches):
        for match in matches:
            message = self.p.sub(match.split("|")[0], message, count=1)
        return message


    def handleMessage(self, data, s):
        if "subtype" in data:
            print "CHANGED MESSAGE WAS NOT HANDLED CORRECTLY"
        else:
            message = ""

            matches = re.findall(self.p, data["text"])
            if matches:
                for match in matches:
                    message = self.replaceUrlTags(data["text"], matches)
            else:
                message = data["text"]

            with db.get_connection() as conn:
                with conn.cursor() as curs:
                    try:
                        curs.execute("INSERT INTO textarchive (message) VALUES (%s);", [message])
                    except psycopg2.Error as e:
                        print e.pgerror
                        pass

