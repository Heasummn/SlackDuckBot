import db
import psycopg2
import re

class LinkHoarder(object):

    p = re.compile(ur'\<(.*?)\>')

    def replaceUrlTags(self, message, matches):
        for match in matches:
            message = self.p.sub(match.split("|")[0], message, count=1)
        return message

    def handleMessage(self, data, s):
        if "subtype" in data:
            print "CHANGED MESSAGE WAS NOT HANDLED CORRECTLY"
        else:
            matches = re.findall(self.p, data["text"])
            if matches:
                for match in matches:
                    username = data["user"]
                    channel = data["channel"]
                    message = self.replaceUrlTags(data["text"], matches)
                    url = match.split("|")[0]

                    if url.startswith(("#", "@")):
                        break

                    with db.get_connection() as conn:
                        with conn.cursor() as curs:
                            try:
                                curs.execute("INSERT INTO links (username, link, message, channel) VALUES (%s, %s, %s, %s);", (username, url, message, channel))
                            except psycopg2.Error as e:
                                print e.pgerror
                                pass