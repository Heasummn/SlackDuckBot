import os.path, sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))
import random
import db
import config
import markovify


class Markov(object):

    TRIGGER = "markov"

    def createSentence(self):
        results = None

        with db.get_connection() as conn:
                    with conn.cursor() as curs:
                        try:
                            curs.execute("SELECT * FROM textarchive;")
                            results = curs.fetchall()
                            results = "\n".join([x[1] for x in results])
                        except psycopg2.Error as e:
                            print e.pgerror
                            return None

        if results:
            text_model = markovify.NewlineText(results)
            sentence = text_model.make_sentence()

            return sentence

    def handleMessage(self, data, s):
        if "subtype" in data:
            print "CHANGED MESSAGE WAS NOT HANDLED CORRECTLY"
        else:
            if data["text"].startswith(config.COMMAND_LEADER + self.TRIGGER):
                s.send_msg(self.createSentence(), channel_name=data["channel"], confirm=False)
            elif random.random() < 0.01:
                s.send_msg(self.createSentence(), channel_name=data["channel"], confirm=False)