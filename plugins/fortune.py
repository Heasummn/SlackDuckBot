import os.path, sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))
import config
import subprocess
from plugin import Plugin
from trigger_handler import TriggerHandler

class Fortune(Plugin):

    def __init__(self):
        self.trigger_handler = TriggerHandler("fortune")

    def on_trigger(self, channel, user, message, s):
        print "Getting random fortune from system"

        response = subprocess.check_output(["fortune"]).strip()

        s.send_msg(response, channel, confirm=False)