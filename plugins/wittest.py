import os.path, sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))
import config
import requests
from urllib import quote_plus
from plugin import Plugin
from trigger_handler import TriggerHandler

class WitHandler(Plugin):

    def __init__(self):
        self.trigger_handler = TriggerHandler("show")

    def bing_search(self, query):
        # Your base API URL; change "Image" to "Web" for web results.
        url = "https://api.datamarket.azure.com/Bing/Search/v1/Image"

        # Query parameters. Don't try using urlencode here.
        # Don't ask why, but Bing needs the "$" in front of its parameters.
        # The '$top' parameter limits the number of search results.
        url += "?$format=json&$top=1&Query=%27{}%27".format(quote_plus(query))

        # You can get your primary account key at https://datamarket.azure.com/account
        r = requests.get(url, auth=("", config.BING_APP_KEY))
        try:
            resp = r.json()
        except Exception:
            resp = None
        return(resp)

    def on_trigger(self, channel, user, message, s):
        search_query = message

        print "Searching bing images for %s" % search_query

        image_search = self.bing_search(search_query)

        if image_search is not None:
            if image_search["d"]["results"]:
                image_url = image_search["d"]["results"][0]["MediaUrl"]
                s.send_msg("@%s: " % user + image_url, channel_name=channel, confirm=False)
                return None  # Exit
        
        print("Searching failed")
        s.send_msg("@%s, I'm sorry. I couldn't find any images for that search" % user, channel_name=channel, confirm=False)