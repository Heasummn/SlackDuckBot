import os.path, sys
sys.path.append(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir))
import config
from urllib2 import urlopen
from bs4 import BeautifulSoup
from plugin import Plugin
from trigger_handler import TriggerHandler

class BashOrg(Plugin):
    def __init__(self):
        self.trigger_handler = TriggerHandler("bash")

    def on_trigger(self, channel, user, message, s):
        print "Getting random bash quote"

        response = urlopen("http://www.bash.org/?random1")
        html = response.read()

        soup = BeautifulSoup(html, "lxml")
        bash_res = soup.find("p", class_="qt").text
        s.send_msg(bash_res, channel, confirm=False)