import random

class Quack(object):

    def handleMessage(self, data, s):
        if "subtype" in data:
            print "CHANGED MESSAGE WAS NOT HANDLED CORRECTLY"
        else:
            if random.random() < 0.01:
                s.send_msg("Quack", channel_name=data["channel"], confirm=False)