# http://stackoverflow.com/questions/6829675/the-proper-method-for-making-a-db-connection-available-across-many-python-module/6830296#6830296

import psycopg2
import config
_connection = None


def get_connection():
    global _connection
    if not _connection:
        _connection = psycopg2.connect(database=config.DATABASE["DBNAME"], user=config.DATABASE["USER"], password=config.DATABASE["PASSWORD"], 
            host=config.DATABASE["HOST"], port=config.DATABASE["PORT"])
    return _connection

# List of stuff accessible to importers of this module. Just in case
__all__ = ['getConnection']