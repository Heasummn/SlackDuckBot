from slacksocket import SlackSocket
import json
import config
from plugins.linkhoarder import LinkHoarder
from plugins.wittest import WitHandler
from plugins.rank import Rank
from plugins.BashOrg import BashOrg
from plugins.simplelogger import SimpleLogger
from plugins.markov import Markov


s = SlackSocket(config.SLACK_TOKEN, translate=True, event_filters=["message"])
# translate will lookup and replace user and channel IDs with their human-readable names. default true.

plugins = [BashOrg(), WitHandler()]

for event in s.events():

    #print(event.json)
    data = json.loads(event.json)

    if "subtype" not in data and data["user"] != config.BOT_NAME:
        channel = data["channel"]
        user = data["user"]
        message = data["text"] 
        trigger = message.split()[0]
        trig_message = " ".join(message.split()[1:])
        for plugin in plugins:
            if plugin.trigger_handler.activated(trigger):
                plugin.on_trigger(channel, user, trig_message, s)
            else:
                plugin.on_message(channel, user, message, s)